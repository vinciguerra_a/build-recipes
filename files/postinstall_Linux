#!/bin/bash

if [[ -z "${OTB_PREFIX}" ]]; then
	echo -e "OTB_PREFIX not set!\nAborting..." 1>&2
	exit 1
fi

if [[ -z $(which patchelf) ]]; then
	echo -e "patchelf missing!\nAborting..." 1>&2
	exit 1
fi

#
# Linux specific post-install
#
postinstall_linux() {
	#
	# cp non-standard libraries required by CMake to tool-chain
	#
	copy_non_standard_system_libs() {
		libs=( $(ldd "${OTB_PREFIX}/bin/ccmake" | awk  '/ => \// && !/'${OTB_PREFIX//\//\\/}'|lib(c|dl|keyutils|m|resolv|pthread|rt|selinux).so/ {print $3}') )

		if (( ${#libs[@]} > 0 )); then
			cp "${libs[@]}" "${OTB_PREFIX}/lib"
		fi
	}
	#
	# strip binaries
	#
	strip_unneeded() {
		for f in "${OTB_PREFIX}/bin/"*; do
			file "$f" | grep -q "ELF 64-bit" && strip --strip-unneeded "$f"
		done
		for f in "${OTB_PREFIX}"/libexec/gcc/x86_64*/*/*; do
			file "$f" | grep -q "ELF 64-bit" && strip --strip-unneeded "$f"
		done
	}

	#
	# set RPATH relative to $ORIGIN in all shared libraries and executables
	# under ${OTB_PREFIX}/{bin,lib}
	#
	echo "Patching RPATH in executables and libraries..."
	while read depth fname; do
		file "${fname}" | egrep -q "ELF 64-bit LSB (shared|executable) " || continue
		local str=$(printf "../%0.s" $(seq 1 $depth))
		local rpath='$ORIGIN/'"${str}lib"
		patchelf --force-rpath --set-rpath "${rpath}" "${fname}"
	done  < <(find "${OTB_PREFIX}/bin" "${OTB_PREFIX}/lib" -type f -printf "%d %p\n" )

	copy_non_standard_system_libs
	strip_unneeded
}

postinstall_linux

